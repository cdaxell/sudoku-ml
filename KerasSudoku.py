import numpy as np
import keras
import SudokuData as SD


(x_train, y_train) = SD.gen_train_data()
(x_test, y_test) = SD.gen_test_data()

np.set_printoptions(precision=5, threshold=50, )

model = keras.models.load_model('sudoku.model')

test_loss, test_acc = model.evaluate(x_train, y_train)

print('Test accuracy:', test_acc)

predictions = model.predict(x_test)
print(predictions[0])
print(np.argmax(predictions[0]))
print(y_test[0])

outputTensor = model.output
listOfVariableTensors = model.trainable_weights
gradients = keras.backend.gradients(outputTensor, listOfVariableTensors)
trainingExample = np.random.random((28, 28, 6000))
print(evaluated_gradients)


