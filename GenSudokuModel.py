import numpy as np
import SudokuData as Sd
import keras
import notMine.Data_pre


def gen_sudoku_model():
    np.set_printoptions(precision=5, threshold=50, )

    #(x_train, x_test, y_train, y_test) = notMine.Data_pre.get_data("sudoku.csv")
    (x_train, y_train) = Sd.gen_train_data()

    Sd.pnt(x_train[1])
    Sd.pnt(y_train[1])
    print(x_train.shape)
    print(y_train.shape)

    model = keras.Sequential([
        keras.layers.Conv2D(64, kernel_size=(3, 3), activation='relu', padding='same', input_shape=(9, 9, 1)),
        keras.layers.BatchNormalization(),
        keras.layers.Conv2D(64, kernel_size=(3, 3), activation='relu', padding='same'),
        keras.layers.BatchNormalization(),
        keras.layers.Conv2D(128, kernel_size=(1, 1), activation='relu', padding='same'),

        keras.layers.Flatten(),
        keras.layers.Dense(81*9),
        keras.layers.Reshape((-1, 9)),
        keras.layers.Activation('softmax')
    ])

    print(model.summary())

    adam = keras.optimizers.adam(lr=.001)
    model.compile(optimizer=adam,
                  loss='sparse_categorical_crossentropy',
                  metrics=['accuracy'])

    print(x_train.shape)
    print(y_train.shape)
    model.fit(x_train, y_train, epochs=2, batch_size=64)

    model.save('sudoku.model')


gen_sudoku_model()