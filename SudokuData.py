import numpy as np
import pandas

np.set_printoptions(precision=5, threshold=50, )


def ita(num):
        res = [int(x) for x in str(num)]
        return res


def formx(arr):
    seq = map(ita, arr)
    return np.reshape(np.array(list(seq)), (arr.size, 9, 9, 1))


def formy(arr):
    seq = map(ita, arr)
    return np.reshape(np.array(list(seq)), (arr.size, 81, 1))


def pnt(arr):
    c = ["", "", "", "", "", "", "", "", ""]
    print(pandas.DataFrame(np.reshape(arr, (9, 9)), columns=c, index=c))


def gen_train_data():
    x = 10000
    dataframe = np.genfromtxt('sudoku.csv', delimiter=",", max_rows=x, skip_header=1, dtype="str")
    x_train = formx(dataframe[:, 0])
    y_train = formy(dataframe[:, 1])
    y_train = y_train-1
    #x_train = x_train/9-0.5
    return x_train, y_train


def gen_test_data():
    x = 1000
    dataframe = np.genfromtxt('sudoku.csv', delimiter=",", max_rows=x, skip_header=10001, dtype="str")
    x_test = formx(dataframe[:, 0])
    #x_test = x_test / 9 - 0.5
    y_test = formy(dataframe[:, 1])
    return x_test, y_test

