import numpy as np
import tensorflow as tf
from tensorflow import keras
from matplotlib import pyplot as plt
from keras import backend as k
import pandas as pd
import math


def form(arr):
    def ita(num):
        res = [int(x) for x in str(num)]
        return res

    seq = map(ita, arr)
    return np.reshape(np.array(list(seq)), (arr.size, 9, 9))


dataframe = np.genfromtxt('sudoku.csv', delimiter=",", max_rows=10000, skip_header=1, dtype="str")
x_train = form(dataframe[:, 0])
y_train = form(dataframe[:, 1])
dataframe = np.genfromtxt('sudoku.csv', delimiter=",", max_rows=1000, skip_header=10001, dtype="str")
x_test = form(dataframe[:, 0])
y_test = form(dataframe[:, 1])

print(x_train.shape)
print(y_train.shape)
print(x_test.shape)
print(y_test.shape)
